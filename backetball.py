import pygame
import sys
from random import randint

class Ball:
    def __init__(self, x = 0, y = 0, speed = 1):
        self.ball = pygame.image.load("basketball.png")
        self.ballrect = self.ball.get_rect()
        self.ballrect.x = x
        self.ballrect.y = y
        self.step_on_x = randint(0, 10)
        self.step_on_y = randint(0, 10)

    def check_borders(self):
        if (self.ballrect.x >= 700):
            self.step_on_x = -1
        if (self.ballrect.x <= 0):
            self.step_on_x = 1
        if (self.ballrect.y >= 500):
            self.step_on_y = -1
        if (self.ballrect.y <= 0):
            self.step_on_y = 1

class Rect:
    def __init__(self):
        self.width = 200
        self.height = 15
        self.x = 300
        self.y = 550
        self.step_on_x = 0

size = width, height = 800, 600
gameover = False
screen = None
bg = None
backetball = Ball(350, 250, 2)
timer = 0
speed = 2
board = Rect()

def draw_ball():
    global timer, speed, backetball
    screen.blit(backetball.ball, backetball.ballrect)
    if (not timer):
        backetball.ballrect.x += backetball.step_on_x
        backetball.ballrect.y += backetball.step_on_y
    timer = (timer + 1) % speed
    backetball.check_borders()

def draw_rect():
    global screen, board
    board.x += board.step_on_x
    if (board.x < 0):
        board.x = 0
    if (board.x > 600):
        board.x = 600
    pygame.draw.rect(screen, (255, 255, 255), (board.x, board.y, board.width, board.height), 0)

def handle_events():
    global gameover, board
    for event in pygame.event.get():
        print(event)
        if event.type == pygame.QUIT:
            gameover = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                board.step_on_x -= 1
            if event.key == pygame.K_d:
                board.step_on_x += 1
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_a:
                board.step_on_x += 1
            if event.key == pygame.K_d:
                board.step_on_x -= 1

def init():
    global screen, bg
    pygame.init()
    bg = (0, 0, 0)
    screen = pygame.display.set_mode(size)

def main():
    global gameover, backetball
    init()
    while not gameover:
        handle_events()
        screen.fill(bg)
        draw_rect()
        draw_ball()
        pygame.display.flip()

if __name__ == '__main__':
    main()